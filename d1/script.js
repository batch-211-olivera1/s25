console.log('World');

// JSON Objects
/*
	-JSON "JavaScript Objext Notation"
	-also used in other programming languages
	-JavaScript objects are not to be confused with JSON
	-Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	-Uses double quotes for property names
	-Syntax:
		{
			"propertyA": "valueA,"
			"propertyB": "valueB,"
		}
*/

// JSON Objects
/*
	{
		"city": "Quezon city",
		"province": "Metro Manila",
		"country": "Philippines"

	}

*/

// JSON Arrays
/*
	
		"cities": [
			{
				"city": "Quezon City", "Province": "Metro Manila", "country": "Philippines"
			}
			{
				"city": "Manila City", "Province": "Metro Manila", "country": "Philippines"
			}
			{
				"city": "Makati City", "Province": "Metro Manila", "country": "Philippines"
			}
		]

*/

// JSON Methods
// The JSON object contains methods for parsing and converting data into stringfield JSON

// Converting data into stringified JSON

/*
	-Stringifield JSON is a JavaScript object converted into a string to be used in other functions of JavaScript application


*/


let batchesArr = [{batchName: "BatchX"}, {batchName: "BatchY"}]

// "Stringify" method is used to convert JavaScript objects into a string
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})
console.log(data);

// Using stringify Method with Variables
/*
	-When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	-this is commonly used when the information to be stored and sent to a frontend application
	-Syntax:

		JSON.stringiy({
			propertyA: variableA,
			propertyB: variableB,
		})
*/
// user details
// // let firstName = prompt('What is your first name?');
// // let lastName = prompt('What is your last name?');
// // let age = prompt('What is your age?');
// // let address = {
// // 	city: prompt('Which city do you live in?'),
// // 	country: prompt('Which country does your city address belong to?')
// // };

// let otherDate = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })
// console.log(otherDate);

// Converting stringify JSON into JavaScript objects

/*
	-Objects are common data types used in applications because of the complex data structures that can be created out of them
	-Information is commonly sent to applications in stringified JSON and then cinverted back into objects
	-This happens both for sending information to a backend application and vice versa
*/

let batchesJSON = `[{ "batchName": "Batch X"}, { "batchName": "Batch Y"}]`;
console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{"name": "John", "age": "31", "address": {"city": "Manila", "country": "Philippines"}}`

console.log(JSON.parse(stringifiedObject));